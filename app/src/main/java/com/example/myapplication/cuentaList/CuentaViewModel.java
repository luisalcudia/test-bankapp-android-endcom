package com.example.myapplication.cuentaList;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.api.ApiInterface;
import com.example.myapplication.api.ApiService;
import com.example.myapplication.api.responses.CuentaResponse;
import com.example.myapplication.models.Cuenta;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CuentaViewModel extends ViewModel {

    private MutableLiveData<Cuenta> cuenta;
    public LiveData<Cuenta> getCuenta() {
        if (cuenta == null) {
            cuenta = new MutableLiveData<Cuenta>();
            loadCuenta();
        }
        return cuenta;
    }

    private void loadCuenta() {
        ApiInterface  apiService = ApiService.getRetrofit().create(ApiInterface.class);

        Call<CuentaResponse> call = apiService.getCuenta();

        call.enqueue(new Callback<CuentaResponse>() {
            @Override
            public void onResponse(Call<CuentaResponse> call, Response<CuentaResponse> response) {

                CuentaResponse cuentaResponse = response.body();
                List<Cuenta> cuentas = cuentaResponse.getCuentas();
                cuenta.setValue(cuentas.get(0));
            }

            @Override
            public void onFailure(Call<CuentaResponse> call, Throwable t) {
                Log.wtf("NO FUNCIONO", t.toString());

            }
        });

    }
}
