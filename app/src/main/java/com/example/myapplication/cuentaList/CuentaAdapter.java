package com.example.myapplication.cuentaList;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.models.Saldo;

import java.util.List;

public class CuentaAdapter extends RecyclerView.Adapter<CuentaAdapter.MyViewHolder> {

    private List<Saldo> cuentas;
    private Context context;

    public CuentaAdapter(List<Saldo> contacts, Context context) {
        this.cuentas = contacts;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_cuentas, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.saldoGeneralText.setText(R.string.saldo_descripcion);
        holder.ingresosText.setText(cuentas.get(position).getSaldoGeneral() +"");
    }

    @Override
    public int getItemCount() {
        return cuentas.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView saldoGeneralText,ingresosText;

        public MyViewHolder(View itemView) {
            super(itemView);

            saldoGeneralText = itemView.findViewById(R.id.saldo_general);
            ingresosText = itemView.findViewById(R.id.ingresos);

        }
    }
}