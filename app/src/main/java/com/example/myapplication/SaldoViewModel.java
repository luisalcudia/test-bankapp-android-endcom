package com.example.myapplication;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.api.ApiInterface;
import com.example.myapplication.api.ApiService;
import com.example.myapplication.api.responses.SaldoResponse;
import com.example.myapplication.models.Cuenta;
import com.example.myapplication.models.Saldo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaldoViewModel extends ViewModel {

    private MutableLiveData<List<Saldo>> _listaSaldo;

    public LiveData<List<Saldo>> getListaSaldo() {
        if (_listaSaldo == null) {
            _listaSaldo = new MutableLiveData<List<Saldo>>();
            getSaldos();
        }
        return _listaSaldo;
    }

    private void getSaldos() {
        ApiInterface apiService = ApiService.getRetrofit().create(ApiInterface.class);

        Call<SaldoResponse> call = apiService.getSaldos();

        call.enqueue(new Callback<SaldoResponse>() {
            @Override
            public void onResponse(Call<SaldoResponse> call, Response<SaldoResponse> response) {
                SaldoResponse saldoResponse = response.body();
                List<Saldo> saldos = saldoResponse.getSaldos();
                _listaSaldo.setValue(saldos);
            }
            @Override
            public void onFailure(Call<SaldoResponse> call, Throwable t) {

            }
        });
    }
}
