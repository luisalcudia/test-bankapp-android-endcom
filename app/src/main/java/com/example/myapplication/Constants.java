package com.example.myapplication;

public class Constants {
    public static final String BASE_URL = "http://bankapp.endcom.mx/api/bankappTest/"; //url bimbo
    public static final String GET_CUENTAS = "cuenta";
    public static final String GET_SALDOS = "saldos";
    public static final String GET_TARJETAS = "tarjetas";
    public static final String GET_MOVIMIENTOS = "movimientos";
}
