package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;


import com.example.myapplication.cuentaList.CuentaAdapter;
import com.example.myapplication.cuentaList.CuentaViewModel;
import com.example.myapplication.databinding.ActivityMainBinding;

import com.example.myapplication.models.Cuenta;
import com.example.myapplication.models.Saldo;
import com.example.myapplication.models.Tarjeta;
import com.example.myapplication.tajertaList.TarjetaAdapter;
import com.example.myapplication.tajertaList.TarjetaViewModel;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private CuentaViewModel cuentaViewModel;
    private SaldoViewModel saldoViewModel;
    private TarjetaViewModel tarjetaViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        cuentaViewModel = new ViewModelProvider(this).get(CuentaViewModel.class);
        saldoViewModel = new ViewModelProvider(this).get(SaldoViewModel.class);
        tarjetaViewModel = new ViewModelProvider(this).get(TarjetaViewModel.class);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.saldoRecycler.setLayoutManager(layoutManager);

        LinearLayoutManager layoutManagerDos = new LinearLayoutManager(this);
        binding.tarjetasRecicler.setLayoutManager(layoutManagerDos);

        binding.loadingProgress.setVisibility(View.VISIBLE);


        final Observer<Cuenta> cuentaObserver = new Observer<Cuenta>() {
            @Override
            public void onChanged(@Nullable final Cuenta newName) {
                binding.includedLayout.setCuenta(newName);
                binding.loadingProgress.setVisibility(View.GONE);

            }
        };

        Observer<List<Saldo>> listaSaldo = new Observer<List<Saldo>>() {
            @Override
            public void onChanged(@Nullable final List<Saldo> newName) {
                Log.wtf("saldo", newName.get(0).getCuenta() + "");

                CuentaAdapter adapter = new CuentaAdapter(newName, MainActivity.this);
                binding.saldoRecycler.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                binding.loadingProgress.setVisibility(View.GONE);

            }
        };

        Observer<List<Tarjeta>> listaTarjeta = new Observer<List<Tarjeta>>() {
            @Override
            public void onChanged(@Nullable final List<Tarjeta> newName) {
                Log.wtf("saldo", newName.get(0).getTarjeta() + "");

                TarjetaAdapter adapter = new TarjetaAdapter(newName, MainActivity.this);
                binding.tarjetasRecicler.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                binding.loadingProgress.setVisibility(View.GONE);

            }
        };
        cuentaViewModel.getCuenta().observe(this, cuentaObserver);
        saldoViewModel.getListaSaldo().observe(this, listaSaldo);
        tarjetaViewModel.getListaTarjetas().observe(this, listaTarjeta);
    }
}