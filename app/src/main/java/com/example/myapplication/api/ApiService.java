package com.example.myapplication.api;

import static com.example.myapplication.Constants.BASE_URL;
import static com.example.myapplication.Constants.GET_CUENTAS;

import com.example.myapplication.api.responses.CuentaResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class ApiService {

    public static Retrofit getRetrofit() {
       return new Retrofit.Builder()

                .baseUrl(BASE_URL)
               .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


}


