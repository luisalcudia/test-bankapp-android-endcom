package com.example.myapplication.api.responses;

import com.example.myapplication.models.Tarjeta;

import java.util.List;

public class TarjetaResponse {
    private List<Tarjeta> tarjetas;

    public List<Tarjeta> getTarjetas() { return tarjetas; }
    public void setTarjetas(List<Tarjeta> value) { this.tarjetas = value; }
}