package com.example.myapplication.api.responses;

import com.example.myapplication.models.Saldo;

import java.util.List;

public class SaldoResponse {
    private List<Saldo> saldos;

    public List<Saldo> getSaldos() { return saldos; }
    public void setSaldos(List<Saldo> value) { this.saldos = value; }
}
