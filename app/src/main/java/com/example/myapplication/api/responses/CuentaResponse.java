package com.example.myapplication.api.responses;

import com.example.myapplication.models.Cuenta;

import java.util.List;

public class CuentaResponse{
    private List<Cuenta> cuenta;
    public List<Cuenta> getCuentas() { return cuenta; }
    public void setCuentas(List<Cuenta> value) { this.cuenta = value; }
}
