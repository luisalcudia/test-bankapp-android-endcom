package com.example.myapplication.api;

import static com.example.myapplication.Constants.GET_CUENTAS;
import static com.example.myapplication.Constants.GET_SALDOS;
import static com.example.myapplication.Constants.GET_TARJETAS;

import com.example.myapplication.api.responses.CuentaResponse;
import com.example.myapplication.api.responses.SaldoResponse;
import com.example.myapplication.api.responses.TarjetaResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET(GET_CUENTAS)
    Call<CuentaResponse> getCuenta();

    @GET(GET_SALDOS)
    Call<SaldoResponse> getSaldos();

    @GET(GET_TARJETAS)
    Call<TarjetaResponse> getTarjetas();
}