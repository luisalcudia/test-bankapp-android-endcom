package com.example.myapplication.tajertaList;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.api.ApiInterface;
import com.example.myapplication.api.ApiService;
import com.example.myapplication.api.responses.SaldoResponse;
import com.example.myapplication.api.responses.TarjetaResponse;
import com.example.myapplication.models.Cuenta;
import com.example.myapplication.models.Saldo;
import com.example.myapplication.models.Tarjeta;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TarjetaViewModel extends ViewModel {

    private MutableLiveData<List<Tarjeta>> _listaTarjetas;

    public LiveData<List<Tarjeta>> getListaTarjetas() {
        if (_listaTarjetas == null) {
            _listaTarjetas = new MutableLiveData<List<Tarjeta>>();
            getTarjetas();
        }
        return _listaTarjetas;
    }

    private void getTarjetas() {
        ApiInterface apiService = ApiService.getRetrofit().create(ApiInterface.class);

        Call<TarjetaResponse> call = apiService.getTarjetas();

        call.enqueue(new Callback<TarjetaResponse>() {
            @Override
            public void onResponse(Call<TarjetaResponse> call, Response<TarjetaResponse> response) {
                TarjetaResponse tarjetaResponse = response.body();
                List<Tarjeta> tarjetas = tarjetaResponse.getTarjetas();
                _listaTarjetas.setValue(tarjetas);
            }
            @Override
            public void onFailure(Call<TarjetaResponse> call, Throwable t) {

            }
        });
    }
}
