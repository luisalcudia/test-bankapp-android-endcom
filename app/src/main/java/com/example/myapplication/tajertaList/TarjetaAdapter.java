package com.example.myapplication.tajertaList;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.models.Saldo;
import com.example.myapplication.models.Tarjeta;

import java.util.List;

public class TarjetaAdapter extends RecyclerView.Adapter<TarjetaAdapter.MyViewHolder> {

    private List<Tarjeta> tarjetas;
    private Context context;

    public TarjetaAdapter(List<Tarjeta> contacts, Context context) {
        this.tarjetas = contacts;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tarjetas, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tarjeta_saldo.setText(tarjetas.get(position).getSaldo() +"");
        holder.tarjeta_estado.setText(tarjetas.get(position).getEstado() +"");
        holder.tarjeta_num_cuenta.setText(tarjetas.get(position).getTarjeta() +"");
        holder.tarjeta_titular.setText(tarjetas.get(position).getNombre() +"");
        holder.tarjeta_tipo.setText(tarjetas.get(position).getTipo() +"");
    }

    @Override
    public int getItemCount() {
        return tarjetas.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tarjeta_saldo,tarjeta_estado,tarjeta_num_cuenta,tarjeta_titular ,tarjeta_tipo;

        public MyViewHolder(View itemView) {
            super(itemView);
            tarjeta_saldo = itemView.findViewById(R.id.tarjeta_saldo);
            tarjeta_estado = itemView.findViewById(R.id.tarjeta_estado);
            tarjeta_num_cuenta = itemView.findViewById(R.id.tarjeta_num_cuenta);
            tarjeta_titular = itemView.findViewById(R.id.tarjeta_titular);
            tarjeta_tipo = itemView.findViewById(R.id.tarjeta_tipo);
        }
    }
}