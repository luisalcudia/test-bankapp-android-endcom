package com.example.myapplication.models;

public class Tarjeta {
    private String tarjeta;
    private String nombre;
    private long saldo;
    private String estado;
    private String tipo;
    private long id;

    public String getTarjeta() { return tarjeta; }
    public void setTarjeta(String value) { this.tarjeta = value; }

    public String getNombre() { return nombre; }
    public void setNombre(String value) { this.nombre = value; }

    public long getSaldo() { return saldo; }
    public void setSaldo(long value) { this.saldo = value; }

    public String getEstado() { return estado; }
    public void setEstado(String value) { this.estado = value; }

    public String getTipo() { return tipo; }
    public void setTipo(String value) { this.tipo = value; }

    public long getID() { return id; }
    public void setID(long value) { this.id = value; }
}
