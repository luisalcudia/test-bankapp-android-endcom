package com.example.myapplication.models;
import com.google.gson.annotations.SerializedName;
public class Cuenta {
    private long cuenta;
    private String nombre;
    private String ultimaSesion;
    private long id;

    public long getCuenta() { return cuenta; }
    public void setCuenta(long value) { this.cuenta = value; }

    public String getNombre() { return nombre; }
    public void setNombre(String value) { this.nombre = value; }

    public String getUltimaSesion() { return ultimaSesion; }
    public void setUltimaSesion(String value) { this.ultimaSesion = value; }

    public long getID() { return id; }
    public void setID(long value) { this.id = value; }
}
