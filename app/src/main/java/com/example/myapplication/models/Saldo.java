package com.example.myapplication.models;

public class Saldo {
    private long cuenta;
    private long saldoGeneral;
    private long ingresos;
    private long gastos;
    private long id;

    public long getCuenta() { return cuenta; }
    public void setCuenta(long value) { this.cuenta = value; }

    public long getSaldoGeneral() { return saldoGeneral; }
    public void setSaldoGeneral(long value) { this.saldoGeneral = value; }

    public long getIngresos() { return ingresos; }
    public void setIngresos(long value) { this.ingresos = value; }

    public long getGastos() { return gastos; }
    public void setGastos(long value) { this.gastos = value; }

    public long getID() { return id; }
    public void setID(long value) { this.id = value; }
}